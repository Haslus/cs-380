
#include <Stdafx.h>
#include "AStar.h"

/**
* @brief 	Constructor for the Node
* @param	pos		Position of the node
* @param	parent 	Parent of the node
*/
AStar::Node::Node(Vec2 cell, float G, float F, Node * parent)
{
	m_cell = cell;
	m_parent = parent;
	m_G = G;
	m_F = F;
}
/**
* @brief 	Function that calls the correct herusitic function
* @param	source
* @param	dest
* @return	Returns the herusitic cost
*/
inline float AStar::Heuristic(Vec2 source, Vec2 dest)
{
	switch (AStar::m_HType)
	{
	case AStar::Heuristic::Manhattan:
		return Heuristic::heuristic_manhattan(source, dest);
		break;
	case AStar::Heuristic::Euclidean:
		return Heuristic::heuristic_euclidean(source, dest);
		break;
	case AStar::Heuristic::Octile:
		return Heuristic::heuristic_octile(source, dest);
		break;
	case AStar::Heuristic::Chebyshev:
		return Heuristic::heuristic_chebyshev(source, dest);
		break;
	default:
		return 0;
	}
}

/**
* @brief 	Generic function to get the delta, used by other heuristic functions
* @param	source
* @param	dest
* @return	The delta
*/
inline Vec2 AStar::Heuristic::GetDelta(Vec2 source, Vec2 dest)
{
	return{ std::abs(source.x - dest.x), std::abs(source.y - dest.y) };
}
/**
* @brief 	Heursitic function of manhattan
* @param	source
* @param	dest
* @return	The heuristic value
*/
inline float AStar::Heuristic::heuristic_manhattan(Vec2 source, Vec2 dest)
{
	Vec2 delta = GetDelta(source, dest);
	return static_cast<float>(delta.x + delta.y);
}
/**
* @brief 	Heursitic function of euclidean
* @param	source
* @param	dest
* @return	The heuristic value
*/
inline float AStar::Heuristic::heuristic_euclidean(Vec2 source, Vec2 dest)
{
	Vec2 delta = GetDelta(source, dest);
	return static_cast<float>(std::sqrt(delta.x * delta.x + delta.y * delta.y));
}
/**
* @brief 	Heursitic function of octile
* @param	source
* @param	dest
* @return	The heuristic value
*/
inline float AStar::Heuristic::heuristic_octile(Vec2 source, Vec2 dest)
{
	Vec2 delta = GetDelta(source, dest);
	return static_cast<float>(min(delta.x,delta.y) * std::sqrt(2) + max(delta.x, delta.y) - min(delta.x, delta.y));
}
/**
* @brief 	Heursitic function of chebyshev
* @param	source
* @param	dest
* @return	The heuristic value
*/
inline float AStar::Heuristic::heuristic_chebyshev(Vec2 source, Vec2 dest)
{
	Vec2 delta = GetDelta(source, dest);
	return static_cast<float>(max(delta.x, delta.y));
}

/**
* @brief 	Finds the shortest path from the source to the destination, using
*			A* search algorithm
* @param	source
* @param	dest
* @param    single_step
* @param    heuristic_mode
* @param    heuristic_weight
* @param    path
* @param	Returns in which state is on (Found, Working, NotFound)
*/
AStar::State AStar::FindPath(Vec2 source, Vec2 dest, bool single_step, int heuristic_mode, float heuristic_weight, Node ** path, bool useAnalysis)
{
	m_HType = AStar::Heuristic::HeuristicType(heuristic_mode);


	if (current == nullptr)
	{
		Node * start = new Node(source);
		//Push start node onto open list
		openList.push_back(start);
	}
	
	//While open list is not empty
	while (openList.empty() == false)
	{
		//Get the node with the lowest F value
		current = openList.front();
		openList.remove(current);
		
		//If we are in the destination, stop, we did it
		if (current->m_cell == dest)
		{
			closedList[current->m_cell.x * g_terrain.GetWidth() + current->m_cell.y] = current;
			*path = current;
			return State::Found;
		}

		//Check adjacent nodes
		for (unsigned i = 0; i < 8; i++)
		{
			Vec2 next_cell = current->m_cell + directions[i];
			float g_x = current->m_G + ((i < 4) ? 1.f : static_cast<float>(std::sqrt(2)));
			if(useAnalysis)			
				g_x += g_terrain.GetInfluenceMapValue(next_cell.x, next_cell.y) * 20.f;
			float f_x = g_x + Heuristic(next_cell, dest) * heuristic_weight;

			//Check if the new position is valid
			if (ValidMovement(current->m_cell, directions[i]) == false)
				continue;

			//If it's on the closed list, we stop
			auto it = closedList.find(next_cell.x * g_terrain.GetWidth() + next_cell.y);
			if (it != closedList.end())
				continue;

			//Search on the openList
			Node* existing_child = FindNode(next_cell);
			//If it isnt on the openlist, create
			if (existing_child == nullptr)
			{
				Node* child = new Node(next_cell, g_x,f_x, current);
				PushNodeOpenList(child);
			}
			//Else if its smaller cost, replace
			else if (existing_child->m_F > f_x)
			{
				openList.remove(existing_child);

				Node* child = new Node(next_cell, g_x, f_x, current);
				PushNodeOpenList(child);
			}
		}
			
			closedList[current->m_cell.x * g_terrain.GetWidth() + current->m_cell.y] = current;
			*path = current;

			if (single_step)
				return State::Working;
		}

		
		
	return State::NotFound;

}

/**
* @brief 	Frees the memory of the nodes
* @return	Parsed file
*/
void AStar::Cleanup()
{
	for (auto it = openList.begin(); it != openList.end(); ++it)
		delete *it;

	for (auto it = closedList.begin(); it != closedList.end(); ++it)
		delete it->second;

	openList.clear();
	closedList.clear();

	current = nullptr;
}
/**
* @brief 	Check if the next movement is valid
* @param	current
* @param	coords
* @return	True if it is
*/
bool AStar::ValidMovement(Vec2 current, Vec2 direction)
{

	Vec2 next_cell = current + direction;

	if (g_terrain.GetWidth() <= next_cell.x || g_terrain.GetWidth() <= next_cell.y
		|| next_cell.x < 0 || next_cell.y < 0)
		return false;

	if (g_terrain.IsWall(next_cell.x, next_cell.y) || g_terrain.IsWall(next_cell.x, current.y) || g_terrain.IsWall(current.x, next_cell.y))
		return false;

	else
		return true;

}

/**
* @brief 	Search the given node by pos
* @param	next_cell
* @return	Return the node, if found
*/
AStar::Node* AStar::FindNode(Vec2 next_cell)
{
	for (auto it = openList.begin(); it != openList.end(); ++it)
		if ((*it)->m_cell == next_cell)
			return *it;

	return nullptr;
	
}
/**
* @brief 	Add node and sort
* @param	next_cell
*/
void AStar::PushNodeOpenList(Node * node)
{
	openList.push_back(node);

	openList.sort( [](Node* a, Node* b) {
		return a->m_F < b->m_F;
	});
}