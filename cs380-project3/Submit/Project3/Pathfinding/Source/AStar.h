#pragma once
#include "Vec2.h"
#include <vector> //std::vector 
#include <string> //std::string 
#include <set> //std::set 
#include <iostream> //std::cout 
#include <cmath> //std::sqrt 
#include <algorithm> //std::count 

#include <Stdafx.h>


class AStar
{
public:

	enum class State
	{
		Found, Working, NotFound
	};

	//A basic node that contains position, a parent and the G and F values
	struct Node
	{
		Node() = default;
		Node(Vec2, float G = 0, float F = 0, Node * parent = nullptr);

		Vec2 m_cell;
		float m_G; //G(X)
		float m_F; //G(X) + H(X)

		Node * m_parent;

		bool operator==(const Node& rhs) const
		{
			return m_cell == rhs.m_cell;
		}
	};

	//Different Heuristic functions
	class Heuristic
	{
	public:
		enum HeuristicType
		{
			Euclidean, Octile, Chebyshev, Manhattan
		};

		float static heuristic_manhattan(Vec2 source, Vec2 dest);
		float static heuristic_euclidean(Vec2 source, Vec2 dest);
		float static heuristic_octile(Vec2 source, Vec2 dest);
		float static heuristic_chebyshev(Vec2 source, Vec2 dest);

	private:

		Vec2 static GetDelta(Vec2 source, Vec2 dest);

	};

	State FindPath(Vec2 source, Vec2 dest, bool single_step, int heuristic_mode, float heuristic_weight, Node ** path, bool useAnalysis);
	void Cleanup();
	bool ValidMovement(Vec2 current, Vec2 direction);
	Node* FindNode(Vec2 next_cell);
	void PushNodeOpenList(Node*);
	float Heuristic(Vec2 source, Vec2 dest);

	Heuristic::HeuristicType m_HType;
	Vec2 directions[8] = { {0,1},{ 1,0 },{ 0,-1 },{ -1,0 },
								{ 1,1 },{ 1,-1 },{ -1,-1 },{ -1,1 } };
	
	std::list<Node*> openList;
	std::unordered_map<int, Node*> closedList;
	Node * current = nullptr;










};
