// Author: Chi-Hao Kuo
// Updated: 12/25/2015

/* Copyright Steve Rabin, 2012. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright Steve Rabin, 2012"
 */

#include <Stdafx.h>
#include "Vec2.h"

void Enemy::FieldOfView(float angle, float closeDistance, float occupancyValue)
{
	// TODO: Implement this for the Occupancy Map project.

	// Parameters:
	//   angle - view cone angle in degree
	//   closeDistance - if the tile is within this distance, no need to check angle
	//   occupancyValue - if the tile is in FOV, apply this value

	// first, clear out old occupancyValue from previous frame
	// (mark m_terrainInfluenceMap[r][c] as zero if the old value is negative

	// For every square on the terrain that is within the field of view cone
	// by the enemy square, mark it with occupancyValue.

	// If the tile is close enough to the enemy (less than closeDistance),
	// you only check if it's visible to the enemy.
	// Otherwise you must also consider the direction the enemy is facing:

	// Get enemy's position and direction
	//   D3DXVECTOR3 pos = m_owner->GetBody().GetPos();
	//   D3DXVECTOR3 dir = m_owner->GetBody().GetDir();

	// Give the enemyDir a field of view a tad greater than the angle:
	//   D3DXVECTOR2 enemyDir = D3DXVECTOR2(dir.x, dir.z);

	// Two grid squares are visible to each other if a line between 
	// their centerpoints doesn't intersect the four boundary lines
	// of every walled grid square. Put this code in IsClearPath().

	// WRITE YOUR CODE HERE
	if (!g_blackboard.GetTerrainAnalysisFlag() ||
		(g_blackboard.GetTerrainAnalysisType() != TerrainAnalysis_HideAndSeek))
		return;

	float** terrainInfluenceMap = g_terrain.GetCurrentMap()->GetInfluenceMap();

	int size = g_terrain.GetWidth();
	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{
			if(terrainInfluenceMap[r][c] < 0.f)
				g_terrain.InitialOccupancyMap(r, c, 0);
		}

	D3DXVECTOR3 pos = m_owner->GetBody().GetPos();
	D3DXVECTOR3 dir = m_owner->GetBody().GetDir();
	int enemy_row, enemy_col;

	g_terrain.GetRowColumn(&pos, &enemy_row, &enemy_col);
	Vec2 enemy_tile{ enemy_row ,enemy_col};
	Vec2 enemyDir = Vec2(dir.x, dir.z);
	enemyDir.Normalize();

	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{

			float distance = std::sqrt((r - enemy_tile.x) * (r - enemy_tile.x) + (c - enemy_tile.y) * (c - enemy_tile.y));

			if (distance < closeDistance)
			{
				if (g_terrain.IsClearPath(enemy_tile.x, enemy_tile.y, r, c))
				{
					g_terrain.InitialOccupancyMap(r, c, occupancyValue);
				}
			}
			else
			{
				Vec2 squareDir {(c - enemy_tile.y),(r - enemy_tile.x) };
				squareDir.Normalize();
				
				float angle_between = std::acos(enemyDir.Dot(squareDir)) * 180.f / std::_Pi;

				if (angle_between <= (angle / 2))
				{
					if (g_terrain.IsClearPath(enemy_tile.x, enemy_tile.y, r, c))
					{
						g_terrain.InitialOccupancyMap(r, c, occupancyValue);
					}
				}
			}
			
		}



	


}

bool Enemy::FindPlayer(void)
{
	// TODO: Implement this for the Occupancy Map project.

	// Check if the player's tile has negative value (FOV cone)
	// Return true if player is within field cone of view of enemy
	// Return false otherwise
	
	// You need to also set new goal to player's position:
	//   ChangeGoal(row_player, col_player);


	// WRITE YOUR CODE HERE
	int player_row = g_blackboard.GetRowPlayer();
	int player_col = g_blackboard.GetColPlayer();

	if (g_terrain.GetCurrentMap()->GetInfluenceMap()[player_row][player_col] < 0.f)
	{
		ChangeGoal(player_row, player_col);
		return true;
	}


	return false;
}

bool Enemy::SeekPlayer(void)
{
	// TODO: Implement this for the Occupancy Map project.

	// Find the tile with 1.0 occupancy value, and set it as 
	// the new goal:
	//   ChangeGoal(row, col);
	// If multiple tiles with 1.0 occupancy, pick the closest
	// tile to the enemy

	// Return false if no tile is found


	// WRITE YOUR CODE HERE
	int row = -1, col = -1;
	float current_distance = -1;

	D3DXVECTOR3 pos = m_owner->GetBody().GetPos();
	int enemy_row, enemy_col;
	g_terrain.GetRowColumn(&pos, &enemy_row, &enemy_col);
	Vec2 enemy_tile{ enemy_row ,enemy_col };

	if (!(enemy_col == m_colGoal) && !(enemy_row == m_rowGoal))
		return true;

	int size = g_terrain.GetWidth();
	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{
			if (g_terrain.GetInfluenceMapValue(r,c) == 1.0f)
			{
				if (row == -1)
				{
					row = r;
					col = c;

					current_distance = std::sqrt((r - enemy_tile.x) * (r - enemy_tile.x) + (c - enemy_tile.y) * (c - enemy_tile.y));
				}
				
				else
				{
					float new_distance = std::sqrt((r - enemy_tile.x) * (r - enemy_tile.x) + (c - enemy_tile.y) * (c - enemy_tile.y));

					if (current_distance > new_distance)
					{
						row = r;
						col = c;

						current_distance = new_distance;
					}

				}
			}
		}

	if (row != -1)
	{
		ChangeGoal(row,col);
		return true;
	}

	return false;
}
