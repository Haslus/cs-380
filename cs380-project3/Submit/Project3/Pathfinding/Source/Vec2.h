#pragma once
//Custom Vector 2
struct Vec2
{
	Vec2() {};
	Vec2(int x_, int y_) : x(static_cast<float>(x_)), y(static_cast<float>(y_)) {};
	Vec2(float x_, float y_) : x(x_), y(y_) {};
	float x, y;

	Vec2 operator+(const Vec2& rhs) const
	{
		return{ rhs.x + x,rhs.y + y };
	};

	void operator+=(const Vec2& rhs)
	{
		x += rhs.x;
		y += rhs.y;
	};
	bool operator==(const Vec2& rhs) const
	{
		return (x == rhs.x) && (y == rhs.y);
	}

	float Dot(const Vec2& rhs)
	{
		return this->x * rhs.x + this->y * rhs.y;
	}

	float Length()
	{
		return std::sqrt(this->x * this->x + this->y * this->y);
	}

	void Normalize()
	{
		float magni = this->Length();
		this->x /= magni;
		this->y /= magni;
	}
};
