/* Copyright Steve Rabin, 2012. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright Steve Rabin, 2012"
 */
#include <Stdafx.h>
#include "AStar.h"

/**
 * @brief 	Adds points after rubberbanding if smooth is ON
 * @param	left
 * @param	right
 * @return	Return the node, if found
 */
void Movement::AddSmoothPoints(std::list<D3DXVECTOR3>::iterator left, std::list<D3DXVECTOR3>::iterator right)
{
	if (D3DXVec3Length(&(*right - *left)) >= m_difference)
	{
		D3DXVECTOR3 midpoint{ ((*left).x + (*right).x) / 2.f, 0, ((*left).z + (*right).z) / 2.f };

		auto it = right;
		m_waypointList.emplace(it, midpoint);
		it--;
		AddSmoothPoints(left, it);
		AddSmoothPoints(it, right);
	}
}

/**
* @brief 	Checks if there is wall between two nodes
* @param	x_
* @param	y_
* @param	x_max
* @param	y_max
* @return	Returns false if there is wall, true otherwise
*/
bool Movement::CheckStraightLine(int x_, int y_, int x_max, int y_max)
{

	for (int x = x_; x <= x_max; x++)
	{
		for (int y = y_; y <= y_max; y++)
		{
			if (g_terrain.IsWall(x, y))
			{
				return false;
			}
		}
	}

	return true;

	
}

/**
* @brief 	Computes the path
* @param	r
* @param	c
* @return	Returns true if a path is found
*/
bool Movement::ComputePath( int r, int c, bool newRequest )
{
	m_goal = g_terrain.GetCoordinates( r, c );
	m_movementMode = MOVEMENT_WAYPOINT_LIST;

	// project 2: change this flag to true
	bool useAStar = true;

	if( useAStar )
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////
		//INSERT YOUR A* CODE HERE
		//1. You should probably make your own A* class.
		//2. You will need to make this function remember the current progress if it preemptively exits.
		//3. Return "true" if the path is complete, otherwise "false".
		///////////////////////////////////////////////////////////////////////////////////////////////////
		int curR, curC;
		D3DXVECTOR3 cur = m_owner->GetBody().GetPos();
		g_terrain.GetRowColumn(&cur, &curR, &curC);

		//If a new request comes, restart AStar
		if (newRequest)
		{
			g_A_Star.Cleanup();
			m_waypointList.clear();
		}

		//Check if straight line
		if (GetStraightlinePath())
		{
			Vec2 delta = { r - curR, c - curC };

			int x = curR > r ? r : curR;
			int y = curC > c ? c : curC;
			int x_max = curR < r ? r : curR;
			int y_max = curC < c ? c : curC;

			bool valid = CheckStraightLine(x,y,x_max,y_max);

			if (valid)
			{
				m_waypointList.push_back(g_terrain.GetCoordinates(r, c));
				return true;
			}
			
		}

		AStar::Node * path = nullptr;
		//Find Path
		AStar::State state = g_A_Star.FindPath(Vec2{ curR,curC }, Vec2{ r,c }, m_singleStep, m_heuristicCalc, m_heuristicWeight, &path, m_aStarUsesAnalysis);
		
		AStar::Node * temp = path;
		//Store nodes
		std::list<AStar::Node*> full_path;
		if(state == AStar::State::Found)
		{
			while (temp)
			{
				full_path.push_front(temp);
				temp = temp->m_parent;
			}

			//Rubberbanding
			if ((full_path.size() > 2) && m_rubberband)
			{
			
				auto current_node = full_path.begin();
				auto next_node = current_node;
				std::advance(next_node,2);
				while (true)
				{

					int x = (*current_node)->m_cell.x > (*next_node)->m_cell.x ? (*next_node)->m_cell.x : (*current_node)->m_cell.x;
					int y = (*current_node)->m_cell.y > (*next_node)->m_cell.y ? (*next_node)->m_cell.y : (*current_node)->m_cell.y;

					int x_max = (*current_node)->m_cell.x < (*next_node)->m_cell.x ? (*next_node)->m_cell.x : (*current_node)->m_cell.x;
					int y_max = (*current_node)->m_cell.y < (*next_node)->m_cell.y ? (*next_node)->m_cell.y : (*current_node)->m_cell.y;

					bool valid = CheckStraightLine(x,y,x_max,y_max);

					//No walls
					if (valid)
					{
						full_path.erase(std::prev(next_node));

						if (std::next(next_node) == full_path.end())
							break;

						next_node++;



					}
					//There is a wall in the path
					else
					{
						if (std::next(next_node) == full_path.end())
							break;

						current_node++;
						next_node++;

					}
				}


			}
			//Store waypoints
			for (auto it = full_path.begin(); it != full_path.end(); it++)
			{
				m_waypointList.push_back(g_terrain.GetCoordinates((*it)->m_cell.x, (*it)->m_cell.y));
			}

			//Add points for smmoothing if rubberband is done
			if (m_rubberband && m_smooth)
			{
				m_difference = 1.5f / (float)g_terrain.GetWidth();
				std::list<D3DXVECTOR3>::iterator it_1 = m_waypointList.begin();
				std::list<D3DXVECTOR3>::iterator it_2 = m_waypointList.begin();
				it_2++;
				unsigned size = m_waypointList.size();
				for (unsigned i = 0; i < size - 1; i++)
				{
					AddSmoothPoints(it_1, it_2);
					it_1 = it_2;
					it_2++;
				}


			}

			//Smoothhing with catmuil
			if (m_smooth)
			{
				//Only two nodes
				if (m_waypointList.size() == 2)
				{
					auto it_current = m_waypointList.begin();
					auto it_1 = m_waypointList.begin();
					auto it_2 = m_waypointList.begin();
					std::advance(it_2, 1);

					D3DXVECTOR3 point_A, point_B, point_C;

					D3DXVec3CatmullRom(&point_A, &*it_1, &*it_1, &*it_2, &*it_2, 0.25f);
					D3DXVec3CatmullRom(&point_B, &*it_1, &*it_1, &*it_2, &*it_2, 0.5f);
					D3DXVec3CatmullRom(&point_C, &*it_1, &*it_1, &*it_2, &*it_2, 0.75f);

					unsigned size = m_waypointList.size();

					m_waypointList.emplace(it_2, point_A);
					m_waypointList.emplace(it_2, point_B);
					m_waypointList.emplace(it_2, point_C);
				}
				//Only three nodes
				else if (m_waypointList.size() == 3)
				{
					auto it_current = m_waypointList.begin();
					auto it_1 = m_waypointList.begin();
					auto it_2 = m_waypointList.begin();
					std::advance(it_2, 1);
					auto it_3 = m_waypointList.begin();
					std::advance(it_3, 2);

					D3DXVECTOR3 point_A, point_B, point_C;

					D3DXVec3CatmullRom(&point_A, &*it_1, &*it_1, &*it_2, &*it_2, 0.25f);
					D3DXVec3CatmullRom(&point_B, &*it_1, &*it_1, &*it_2, &*it_2, 0.5f);
					D3DXVec3CatmullRom(&point_C, &*it_1, &*it_1, &*it_2, &*it_2, 0.75f);

					unsigned size = m_waypointList.size();

					m_waypointList.emplace(it_2, point_A);
					m_waypointList.emplace(it_2, point_B);
					m_waypointList.emplace(it_2, point_C);

					D3DXVec3CatmullRom(&point_A, &*it_2, &*it_2, &*it_3, &*it_3, 0.25f);
					D3DXVec3CatmullRom(&point_B, &*it_2, &*it_2, &*it_3, &*it_3, 0.5f);
					D3DXVec3CatmullRom(&point_C, &*it_2, &*it_2, &*it_3, &*it_3, 0.75f);

					m_waypointList.emplace(it_3, point_A);
					m_waypointList.emplace(it_3, point_B);
					m_waypointList.emplace(it_3, point_C);
				}

				//Four or more nodes
				else
				{
					auto it_current = m_waypointList.begin();
					auto it_1 = m_waypointList.begin();
					auto it_2 = m_waypointList.begin();
					std::advance(it_2, 1);
					auto it_3 = m_waypointList.begin();
					std::advance(it_3, 2);
					auto it_4 = m_waypointList.begin();
					std::advance(it_4, 3);

					//Start case
					D3DXVECTOR3 point_A, point_B, point_C;

					D3DXVec3CatmullRom(&point_A, &*it_1, &*it_1, &*it_2, &*it_3, 0.25f);
					D3DXVec3CatmullRom(&point_B, &*it_1, &*it_1, &*it_2, &*it_3, 0.5f);
					D3DXVec3CatmullRom(&point_C, &*it_1, &*it_1, &*it_2, &*it_3, 0.75f);

					unsigned size = m_waypointList.size();

					m_waypointList.emplace(it_2, point_A);
					m_waypointList.emplace(it_2, point_B);
					m_waypointList.emplace(it_2, point_C);


					for (unsigned i = 1; i < size - 1; i++)
					{
						D3DXVec3CatmullRom(&point_A, &*it_1, &*it_2, &*it_3, &*it_4, 0.25f);
						D3DXVec3CatmullRom(&point_B, &*it_1, &*it_2, &*it_3, &*it_4, 0.5f);
						D3DXVec3CatmullRom(&point_C, &*it_1, &*it_2, &*it_3, &*it_4, 0.75f);

						it_current = it_3;

						m_waypointList.emplace(it_current, point_A);
						m_waypointList.emplace(it_current, point_B);
						m_waypointList.emplace(it_current, point_C);

						if (std::next(it_4) == m_waypointList.end())
							break;

						it_1 = it_2;
						it_2 = it_3;
						it_3 = it_4;
						it_4++;


					}

					//End case
					D3DXVec3CatmullRom(&point_A, &*it_2, &*it_3, &*it_4, &*it_4, 0.25f);
					D3DXVec3CatmullRom(&point_B, &*it_2, &*it_3, &*it_4, &*it_4, 0.5f);
					D3DXVec3CatmullRom(&point_C, &*it_2, &*it_3, &*it_4, &*it_4, 0.75f);

					m_waypointList.emplace(it_4, point_A);
					m_waypointList.emplace(it_4, point_B);
					m_waypointList.emplace(it_4, point_C);
				}


			}
		}

		//Paint for debugging

		if (m_debugDraw)
		{
			for (auto it = g_A_Star.openList.begin(); it != g_A_Star.openList.end(); ++it)
				g_terrain.SetColor((*it)->m_cell.x, (*it)->m_cell.y, DEBUG_COLOR_BLUE);

			for (auto it = g_A_Star.closedList.begin(); it != g_A_Star.closedList.end(); ++it)
				g_terrain.SetColor(it->second->m_cell.x, it->second->m_cell.y, DEBUG_COLOR_YELLOW);
		}
		
		
		return state == AStar::State::Found ? true : false;

	}
	else
	{	
		//Randomly meander toward goal (might get stuck at wall)
		int curR, curC;
		D3DXVECTOR3 cur = m_owner->GetBody().GetPos();
		g_terrain.GetRowColumn( &cur, &curR, &curC );

		m_waypointList.clear();
		m_waypointList.push_back( cur );

		int countdown = 100;
		while( curR != r || curC != c )
		{
			if( countdown-- < 0 ) { break; }

			if( curC == c || (curR != r && rand()%2 == 0) )
			{	//Go in row direction
				int last = curR;
				if( curR < r ) { curR++; }
				else { curR--; }

				if( g_terrain.IsWall( curR, curC ) )
				{
					curR = last;
					continue;
				}
			}
			else
			{	//Go in column direction
				int last = curC;
				if( curC < c ) { curC++; }
				else { curC--; }

				if( g_terrain.IsWall( curR, curC ) )
				{
					curC = last;
					continue;
				}
			}

			D3DXVECTOR3 spot = g_terrain.GetCoordinates( curR, curC );
			m_waypointList.push_back( spot );
			g_terrain.SetColor( curR, curC, DEBUG_COLOR_YELLOW );
		}
		return true;
	}
}
