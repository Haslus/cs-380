/* Copyright Steve Rabin, 2012.
* All rights reserved worldwide.
*
* This software is provided "as is" without express or implied
* warranties. You may freely copy and compile this source into
* applications you distribute provided that the copyright text
* below is included in the resulting source code, for example:
* "Portions Copyright Steve Rabin, 2012"
*/

#include <Stdafx.h>


float Terrain::ClosestWall(int row, int col)
{
	// TODO: Helper function for the Terrain Analysis project.

	// For each tile, check the distance with every wall of the map.
	// Return the distance to the closest wall or side.


	// WRITE YOUR CODE HERE

	int size = this->GetWidth();
	float min_distance = FLT_MAX;
	for(int r = -1; r <= size; r++)
		for (int c = -1; c <= size; c++)
		{
			if (row == r && col == c)
				continue;

			if (r == -1 || c == -1 || r == size || c == size || IsWall(r, c))
			{
				float current_distance = std::sqrt((r - row) * (r - row) + (c - col) * (c - col));
				if (current_distance < min_distance)
					min_distance = current_distance;
			}
			
		}


	return min_distance;	//REPLACE THIS
}

void Terrain::AnalyzeOpennessClosestWall(void)
{
	// TODO: Implement this for the Terrain Analysis project.

	// Mark every square on the terrain (m_terrainInfluenceMap[r][c]) with
	// the value 1/(d*d), where d is the distance to the closest wall in 
	// row/column grid space.
	// Edges of the map count as walls!

	// WRITE YOUR CODE HERE
	int size = this->GetWidth();

	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{
			float d = ClosestWall(r, c);
			m_terrainInfluenceMap[r][c] = 1.f / (d*d);
		}



}

void Terrain::AnalyzeVisibility(void)
{
	// TODO: Implement this for the Terrain Analysis project.

	// Mark every square on the terrain (m_terrainInfluenceMap[r][c]) with
	// the number of grid squares (that are visible to the square in question)
	// divided by 160 (a magic number that looks good). Cap the value at 1.0.

	// Two grid squares are visible to each other if a line between 
	// their centerpoints doesn't intersect the four boundary lines
	// of every walled grid square. Put this code in IsClearPath().


	// WRITE YOUR CODE HERE
	int size = this->GetWidth();

	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{
			float counter = 0;
			for (int other_r = 0; other_r < size; other_r++)
				for (int other_c = 0; other_c < size; other_c++)
				{
					if (IsClearPath(r, c, other_r, other_c))
						counter++;
				}


			m_terrainInfluenceMap[r][c] = counter / 160.f;
		}



}

void Terrain::AnalyzeVisibleToPlayer(void)
{
	// TODO: Implement this for the Terrain Analysis project.

	// For every square on the terrain (m_terrainInfluenceMap[r][c])
	// that is visible to the player square, mark it with 1.0.
	// For all non 1.0 squares that are visible to, and next to 1.0 squares,
	// mark them with 0.5. Otherwise, the square should be marked with 0.0.

	// Two grid squares are visible to each other if a line between 
	// their centerpoints doesn't intersect the four boundary lines
	// of every walled grid square. Put this code in IsClearPath().


	// WRITE YOUR CODE HERE

	m_terrainInfluenceMap[m_rPlayer][m_cPlayer] = 1.0f;
	std::vector<D3DXVECTOR2> visible_tiles;
	int size = GetWidth();

	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
			m_terrainInfluenceMap[r][c] = 0;
	
	for(int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{
			if (IsClearPath(m_rPlayer, m_cPlayer, r, c))
			{
				m_terrainInfluenceMap[r][c] = 1.0f;
				visible_tiles.push_back({ static_cast<float>(r),static_cast<float>(c)});
			}
			else
			{
				m_terrainInfluenceMap[r][c] = 0.0f;
			}
		}

	static const D3DXVECTOR2 dir[8] = { {1,0},{ 1,1 },{ 0,1 },{ 0,-1 },{ -1,0 },{ -1,1 },{ -1,-1 },{ 1,-1 } };

	for (int i = 0; i < visible_tiles.size(); i++)
	{
		int row = visible_tiles[i].x;
		int col = visible_tiles[i].y;

		for (int j = 0; j < 8; j++)
		{
			int next_row = row + dir[j].x;
			int next_col = col + dir[j].y;
			if (next_row >= 0 && next_row < GetWidth() && next_col >= 0 && next_col < GetWidth() && m_terrainInfluenceMap[next_row][next_col] != 1.0f && IsClearPath(row,col,next_row,next_col))
			{
				m_terrainInfluenceMap[next_row][next_col] = 0.5f;
			}
		}

	}



}

void Terrain::AnalyzeSearch(void)
{
	// TODO: Implement this for the Terrain Analysis project.

	// For every square on the terrain (m_terrainInfluenceMap[r][c])
	// that is visible by the player square, mark it with 1.0.
	// Otherwise, don't change the value (because it will get
	// decremented with time in the update call).
	// You must consider the direction the player is facing:
	// D3DXVECTOR2 playerDir = D3DXVECTOR2(m_dirPlayer.x, m_dirPlayer.z)
	// Give the player a field of view a tad greater than 180 degrees.

	// Two grid squares are visible to each other if a line between 
	// their centerpoints doesn't intersect the four boundary lines
	// of every walled grid square. Put this code in IsClearPath().


	// WRITE YOUR CODE HERE
	m_terrainInfluenceMap[m_rPlayer][m_cPlayer] = 1.0f;
	D3DXVECTOR2 playerDir = D3DXVECTOR2(m_dirPlayer.x, m_dirPlayer.z);

	int size = GetWidth();
	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{
			D3DXVECTOR2 squareDir = D3DXVECTOR2{ static_cast<float>(c - m_cPlayer),static_cast<float>(r - m_rPlayer) };
			D3DXVec2Normalize(&squareDir, &squareDir);

			float angle = D3DXVec2Dot(&playerDir,&squareDir);

			if (angle >= 0.f)
			{
				if (IsClearPath(m_rPlayer, m_cPlayer, r, c))
				{
					m_terrainInfluenceMap[r][c] = 1.0f;
				}
			}

		}

}

bool Terrain::IsClearPath(int r0, int c0, int r1, int c1)
{
	// TODO: Implement this for the Terrain Analysis project.

	// Two grid squares (r0,c0) and (r1,c1) are visible to each other 
	// if a line between their centerpoints doesn't intersect the four 
	// boundary lines of every walled grid square. Make use of the
	// function LineIntersect(). You should also puff out the four
	// boundary lines by a very tiny bit so that a diagonal line passing
	// by the corner will intersect it.


	// WRITE YOUR CODE HERE
	int min_r, min_c, max_r, max_c;

	if (r0 < r1)
	{
		min_r = r0;
		max_r = r1;
	}
	else
	{
		min_r = r1;
		max_r = r0;
	}

	if (c0 < c1)
	{
		min_c = c0;
		max_c = c1;
	}
	else
	{
		min_c = c1;
		max_c = c0;
	}
	float size = GetWidth();
	float epsilon = size / 1000.f;
	D3DXVECTOR2 edges[4] = { {-0.5f - epsilon, -0.5f - epsilon },{ -0.5f - epsilon, 0.5f + epsilon }, {0.5f + epsilon, 0.5f + epsilon }, {0.5f + epsilon,-0.5f - epsilon }};

	for(int r = min_r; r <= max_r; r++)
		for (int c = min_c; c <= max_c; c++)
		{
			if (IsWall(r, c))
			{
				for (int i = 0; i < 4; i++)
				{
					if (LineIntersect(r0, c0, r1, c1, r + edges[i].x, c + edges[i].y, r + edges[(i + 1) % 3].x, c + edges[(i + 1) % 3].y) == true)
						return false;
				}
			}
		}


	return true;	//REPLACE THIS
}

void Terrain::Propagation(float decay, float growing, bool computeNegativeInfluence)
{
	// TODO: Implement this for the Occupancy Map project.

	// computeNegativeInfluence flag is true if we need to handle two agents
	// (have both positive and negative influence)
	// computeNegativeInfluence flag is false if we only deal with positive
	// influence (we should ignore all negative influence)

	// Pseudo code:
	//
	// For each tile on the map
	//
	//   Get the influence value of each neighbor after decay
	//   Then keep the decayed influence WITH THE HIGHEST ABSOLUTE.
	//
	//   Apply linear interpolation to the influence value of the tile, 
	//   and the maximum decayed influence value from all neighbors, with growing 
	//   factor as coefficient
	//
	//   Store the result to the temp layer
	//
	// Store influence value from temp layer

	// WRITE YOUR CODE HERE
	int size = GetWidth();
	std::vector<float> cols(size, 0);

	std::vector<std::vector<float>> temp_layer(size, cols);

	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{
			if (IsWall(r, c))
				continue;

			float max_influence = 0;

			for (int dir_x = -1; dir_x <= 1; dir_x++)
				for (int dir_y = -1; dir_y <= 1; dir_y++)
				{
					if (dir_x == 0 && dir_y == 0)
						continue;


					int next_row = r + dir_x;
					int next_col = c + dir_y;

					if (next_row < 0 || next_row >= size ||
						next_col < 0 || next_col >= size)
						continue;

					if (IsWall(next_row, next_col))
						continue;

					float old_influence = m_terrainInfluenceMap[next_row][next_col] * std::exp(-1 * std::sqrt(2) * decay);


					if (std::abs(old_influence) > std::abs(max_influence))
						max_influence = old_influence;


				}


			temp_layer[r][c] = (1 - growing) *  m_terrainInfluenceMap[r][c] + growing * max_influence;

		}

	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{
			if (!computeNegativeInfluence && temp_layer[r][c] < 0.f)
				continue;

			InitialOccupancyMap(r,c,temp_layer[r][c]);
		}

	
}

void Terrain::NormalizeOccupancyMap(bool computeNegativeInfluence)
{
	// TODO: Implement this for the Occupancy Map project.

	// divide all tiles with maximum influence value, so the range of the
	// influence is kept in [0,1]
	// if we need to handle negative influence value, divide all positive
	// tiles with maximum influence value, and all negative tiles with
	// minimum influence value * -1, so the range of the influence is kept
	// at [-1,1]
	
	// computeNegativeInfluence flag is true if we need to handle two agents
	// (have both positive and negative influence)
	// computeNegativeInfluence flag is false if we only deal with positive
	// influence, ignore negative influence 

	// WRITE YOUR CODE HERE
	int size = GetWidth();

	float highest_positive_inf = 0, lowest_negative_inf = 0;
	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{
			float current_inf = m_terrainInfluenceMap[r][c];

			if (current_inf > highest_positive_inf)
			{
				highest_positive_inf = current_inf;
			}

			else if (computeNegativeInfluence && current_inf < 0 && lowest_negative_inf > current_inf)
			{
				lowest_negative_inf = current_inf;
			}
		}

	for (int r = 0; r < size; r++)
		for (int c = 0; c < size; c++)
		{
			if (computeNegativeInfluence)
			{
				float current_inf = m_terrainInfluenceMap[r][c];

				if (m_terrainInfluenceMap[r][c] > 0 && highest_positive_inf != 0)
				{
					InitialOccupancyMap(r, c, (m_terrainInfluenceMap[r][c] / highest_positive_inf));
				}

				else if(lowest_negative_inf != 0)
				{
					InitialOccupancyMap(r, c, (m_terrainInfluenceMap[r][c] / -lowest_negative_inf));
				}
			}

			else if(highest_positive_inf != 0 && m_terrainInfluenceMap[r][c] > 0.f)
			{
				InitialOccupancyMap(r,c,(m_terrainInfluenceMap[r][c] / highest_positive_inf));
			}
		}

}
