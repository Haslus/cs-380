Student Name: Asier Bilbao

Special Directions (if any): None

Missing features (if any): None

My experience working on this project:It's iinteresting to see how a simple "Hide and Seek" AI behaves.

Hours spent: 20 hours
Hide and Seek took most of my time.

Extra credits: None

Comment: I don't know if there were tests, so I just compared with your example. I also tested the
A* speed with the reference you gave, and mine was always a bit faster.

Also toggle analysis may crash when it's still working, and this also happens in your example, so I suppose that it's OK.